extern crate rand;
extern crate regex;
#[macro_use]
extern crate structopt;

use rand::Rng;
use regex::Regex;
use std::collections::{BTreeMap, BTreeSet, VecDeque};
use std::env;
use std::error::Error;
use std::fs;
use std::path::{Path, PathBuf};
use std::sync::mpsc::{channel, Receiver, Sender};
use std::sync::{Arc, Mutex};
use std::thread::{self, JoinHandle};
use structopt::StructOpt;

type Result<T> = std::result::Result<T, Box<Error>>;

const PRIVATE_ID: &str = "268a409212ae495792330b828daaf5d6";

mod vector {
    use std::ops::{Add, AddAssign, Sub, SubAssign};

    pub type Pos = (i32, i32, i32);

    pub trait Vector {
        fn pos(&self) -> Pos;
        fn from_pos(Pos) -> Self;
        fn mlen(&self) -> i64 {
            let (x, y, z) = self.pos();
            (x.abs() + y.abs() + z.abs()) as i64
        }
    }

    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    pub struct LLD(i32, i32, i32);

    impl LLD {
        pub fn new(x: i32, y: i32, z: i32) -> LLD {
            assert!(
                (x != 0 && y == 0 && z == 0)
                    || (x == 0 && y != 0 && z == 0)
                    || (x == 0 && y == 0 && z != 0),
                "LLD: out of range: {:?}",
                (x, y, z)
            );
            assert!(x.abs() <= 15);
            assert!(y.abs() <= 15);
            assert!(z.abs() <= 15);

            LLD(x, y, z)
        }

        pub fn decode(axis: u8, dist: u8) -> LLD {
            let dist = dist as i32 - 15;
            match axis {
                0b01 => LLD::new(dist, 0, 0),
                0b10 => LLD::new(0, dist, 0),
                0b11 => LLD::new(0, 0, dist),
                _ => unreachable!(),
            }
        }

        pub fn encode(&self) -> (u8, u8) {
            if self.0 != 0 {
                (0b01, (self.0 + 15) as u8)
            } else if self.1 != 0 {
                (0b10, (self.1 + 15) as u8)
            } else if self.2 != 0 {
                (0b11, (self.2 + 15) as u8)
            } else {
                unreachable!()
            }
        }
    }

    impl Vector for LLD {
        fn pos(&self) -> Pos {
            (self.0, self.1, self.2)
        }
        fn from_pos(p: Pos) -> Self {
            Self::new(p.0, p.1, p.2)
        }
    }

    #[derive(Debug, Clone, PartialEq, Eq)]
    pub struct SLD(i32, i32, i32);

    impl SLD {
        pub fn new(x: i32, y: i32, z: i32) -> SLD {
            assert!(
                (x != 0 && y == 0 && z == 0)
                    || (x == 0 && y != 0 && z == 0)
                    || (x == 0 && y == 0 && z != 0)
            );
            assert!(x.abs() <= 5);
            assert!(y.abs() <= 5);
            assert!(z.abs() <= 5);

            SLD(x, y, z)
        }

        pub fn decode(axis: u8, dist: u8) -> SLD {
            let dist = dist as i32 - 5;
            match axis {
                0b01 => SLD::new(dist, 0, 0),
                0b10 => SLD::new(0, dist, 0),
                0b11 => SLD::new(0, 0, dist),
                _ => unreachable!(),
            }
        }

        pub fn encode(&self) -> (u8, u8) {
            if self.0 != 0 {
                (0b01, (self.0 + 5) as u8)
            } else if self.1 != 0 {
                (0b10, (self.1 + 5) as u8)
            } else if self.2 != 0 {
                (0b11, (self.2 + 5) as u8)
            } else {
                unreachable!()
            }
        }
    }

    impl Vector for SLD {
        fn pos(&self) -> Pos {
            (self.0, self.1, self.2)
        }
        fn from_pos(p: Pos) -> Self {
            Self::new(p.0, p.1, p.2)
        }
    }

    #[derive(Debug, Clone, PartialEq, Eq)]
    pub struct ND(i32, i32, i32);

    impl ND {
        pub fn new(x: i32, y: i32, z: i32) -> ND {
            assert!(x.abs() <= 1);
            assert!(y.abs() <= 1);
            assert!(z.abs() <= 1);
            assert!(
                x.abs() + y.abs() + z.abs() <= 2,
                "mlen(d) must <= 2, {:?}",
                (x, y, z)
            );
            assert!(x.abs() + y.abs() + z.abs() >= 1);
            ND(x, y, z)
        }

        pub fn decode(b: u8) -> ND {
            let dx = (b / 9) as i32 - 1;
            let dy = (b / 3 % 3) as i32 - 1;
            let dz = (b % 3) as i32 - 1;
            ND::new(dx, dy, dz)
        }

        pub fn encode(&self) -> u8 {
            ((self.0 + 1) * 9 + (self.1 + 1) * 3 + (self.2 + 1)) as u8
        }
    }

    impl Vector for ND {
        fn pos(&self) -> Pos {
            (self.0, self.1, self.2)
        }
        fn from_pos(p: Pos) -> Self {
            Self::new(p.0, p.1, p.2)
        }
    }

    #[derive(Debug, Clone, PartialEq, Eq)]
    pub struct FD(i32, i32, i32);

    impl FD {
        pub fn new(x: i32, y: i32, z: i32) -> FD {
            assert!(x.abs() <= 30);
            assert!(y.abs() <= 30);
            assert!(z.abs() <= 30);
            FD(x, y, z)
        }

        pub fn decode(b: &[u8; 3]) -> FD {
            let x = b[0] as i32 - 30;
            let y = b[1] as i32 - 30;
            let z = b[2] as i32 - 30;
            FD::new(x, y, z)
        }

        pub fn encode(&self) -> [u8; 3] {
            [
                (self.0 + 30) as u8,
                (self.1 + 30) as u8,
                (self.2 + 30) as u8,
            ]
        }
    }

    impl Vector for FD {
        fn pos(&self) -> Pos {
            (self.0, self.1, self.2)
        }
        fn from_pos(p: Pos) -> Self {
            Self::new(p.0, p.1, p.2)
        }
    }

    macro_rules! def_ops {
        ($t:ty) => {
            impl AddAssign<$t> for Pos {
                fn add_assign(&mut self, rhs: $t) {
                    self.0 += rhs.0;
                    self.1 += rhs.1;
                    self.2 += rhs.2;
                }
            }

            impl Add<$t> for Pos {
                type Output = Pos;
                fn add(mut self, rhs: $t) -> Self::Output {
                    self += rhs;
                    self
                }
            }

            impl<'a> Add<&'a $t> for Pos {
                type Output = Pos;
                fn add(mut self, rhs: &$t) -> Self::Output {
                    self += rhs.clone();
                    self
                }
            }

            impl SubAssign<$t> for Pos {
                fn sub_assign(&mut self, rhs: $t) {
                    self.0 -= rhs.0;
                    self.1 -= rhs.1;
                    self.2 -= rhs.2;
                }
            }

            impl Sub<$t> for Pos {
                type Output = Pos;
                fn sub(mut self, rhs: $t) -> Self::Output {
                    self -= rhs;
                    self
                }
            }
        };
    }

    def_ops!(LLD);
    def_ops!(SLD);
    def_ops!(ND);
    def_ops!(FD);
}

use vector::*;

#[derive(Debug, Clone, PartialEq, Eq)]
enum Command {
    Halt,
    Wait,
    Flip,
    SMove(LLD),
    LMove(SLD, SLD),
    FusionP(ND),
    FusionS(ND),
    Fission(ND, usize),
    Fill(ND),

    // Full
    Void(ND),
    GFill(ND, FD),
    GVoid(ND, FD),

    // Pseudo command
    Barrier,
}
use Command::*;

fn head(b: &[u8]) -> (u8, &[u8]) {
    (b[0], &b[1..])
}

impl Command {
    fn decode(b: &[u8]) -> Option<(Command, &[u8])> {
        if b.len() == 0 {
            return None;
        }

        let (c, b) = head(b);

        Some(if c == 0b11111111 {
            (Halt, b)
        } else if c == 0b11111110 {
            (Wait, b)
        } else if c == 0b11111101 {
            (Flip, b)
        } else if c & 0b11001111 == 0b00000100 {
            let (c2, b) = head(b);
            assert!(c2 & 0b11100000 == 0b00000000);
            (SMove(LLD::decode((c >> 4) & 0b11, c2 & 0b11111)), b)
        } else if c & 0b00001111 == 0b00001100 {
            let (c2, b) = head(b);
            let sld1 = SLD::decode((c >> 4) & 0b11, c2 & 0b1111);
            let sld2 = SLD::decode(c >> 6, c2 >> 4);
            (LMove(sld1, sld2), b)
        } else if c & 0b00000111 == 0b00000111 {
            let nd = ND::decode(c >> 3);
            (FusionP(nd), b)
        } else if c & 0b00000111 == 0b00000110 {
            let nd = ND::decode(c >> 3);
            (FusionS(nd), b)
        } else if c & 0b00000111 == 0b00000101 {
            let (c2, b) = head(b);
            let nd = ND::decode(c >> 3);
            (Fission(nd, c2 as usize), b)
        } else if c & 0b00000111 == 0b00000011 {
            let nd = ND::decode(c >> 3);
            (Fill(nd), b)
        } else if c & 0b00000111 == 0b00000010 {
            let nd = ND::decode(c >> 3);
            (Void(nd), b)
        } else if c & 0b00000111 == 0b00000001 {
            let nd = ND::decode(c >> 3);
            let (b0, b) = head(b);
            let (b1, b) = head(b);
            let (b2, b) = head(b);
            let fd = FD::decode(&[b0, b1, b2]);
            (GFill(nd, fd), b)
        } else if c & 0b00000111 == 0b00000000 {
            let nd = ND::decode(c >> 3);
            let (b0, b) = head(b);
            let (b1, b) = head(b);
            let (b2, b) = head(b);
            let fd = FD::decode(&[b0, b1, b2]);
            (GVoid(nd, fd), b)
        } else {
            unreachable!("Opcode: {:02x}", c)
        })
    }

    fn encode(&self, ret: &mut Vec<u8>) {
        match self {
            Halt => ret.push(0b11111111),
            Wait => ret.push(0b11111110),
            Flip => ret.push(0b11111101),

            SMove(lld) => {
                let (a, b) = lld.encode();
                ret.push(0b00000100 | a << 4);
                ret.push(b);
            }
            LMove(sld1, sld2) => {
                let (a1, b1) = sld1.encode();
                let (a2, b2) = sld2.encode();
                ret.push(0b00001100 | a1 << 4 | a2 << 6);
                ret.push(b2 << 4 | b1);
            }
            FusionP(nd) => {
                ret.push(nd.encode() << 3 | 0b111);
            }
            FusionS(nd) => {
                ret.push(nd.encode() << 3 | 0b110);
            }
            Fission(nd, m) => {
                ret.push(nd.encode() << 3 | 0b101);
                ret.push(*m as u8);
            }
            Fill(nd) => {
                ret.push(nd.encode() << 3 | 0b011);
            }
            Void(nd) => {
                ret.push(nd.encode() << 3 | 0b010);
            }
            GFill(nd, fd) => {
                ret.push(nd.encode() << 3 | 0b001);
                let t = fd.encode();
                ret.push(t[0]);
                ret.push(t[1]);
                ret.push(t[2]);
            }
            GVoid(nd, fd) => {
                ret.push(nd.encode() << 3 | 0b000);
                let t = fd.encode();
                ret.push(t[0]);
                ret.push(t[1]);
                ret.push(t[2]);
            }

            _ => unreachable!(),
        }
    }

    fn volatile_pos(&self, pos: &Pos) -> Vec<Pos> {
        match self {
            Halt | Wait | Flip => vec![*pos],

            SMove(lld) => line_range(*pos, *pos + *lld),
            LMove(sld1, sld2) => {
                let mut a = line_range(*pos, *pos + sld1);
                let mut b = line_range(*pos + sld1, *pos + sld1 + sld2);
                a.append(&mut b);
                a
            }
            Fission(nd, _m) => vec![*pos, *pos + nd],
            Fill(nd) => vec![*pos, *pos + nd],
            Void(nd) => vec![*pos, *pos + nd],
            FusionP(_nd) => vec![*pos],
            FusionS(_nd) => vec![*pos],

            GFill(nd, fd) => {
                let p = *pos + nd;
                let q = p + fd;
                let (r, s) = normalize_range(&p, &q);

                let mut ret = vec![];
                for x in r.0..=s.0 {
                    for y in r.1..=s.1 {
                        for z in r.2..=s.2 {
                            ret.push((x, y, z));
                        }
                    }
                }
                ret
            }

            GVoid(nd, fd) => {
                let p = *pos + nd;
                let q = p + fd;
                let (r, s) = normalize_range(&p, &q);

                let mut ret = vec![];
                for x in r.0..=s.0 {
                    for y in r.1..=s.1 {
                        for z in r.2..=s.2 {
                            ret.push((x, y, z));
                        }
                    }
                }
                ret
            }

            _ => unreachable!(),
        }
    }
}

fn line_range(p: Pos, q: Pos) -> Vec<Pos> {
    if p.0 != q.0 {
        (p.0.min(q.0)..=p.0.max(q.0))
            .map(|i| (i, p.1, p.2))
            .collect()
    } else if p.1 != q.1 {
        (p.1.min(q.1)..=p.1.max(q.1))
            .map(|i| (p.0, i, p.2))
            .collect()
    } else if p.2 != q.2 {
        (p.2.min(q.2)..=p.2.max(q.2))
            .map(|i| (p.0, p.1, i))
            .collect()
    } else {
        unreachable!()
    }
}

fn decode_commands(b: &[u8]) -> Vec<Command> {
    let mut b = b;
    let mut ret = vec![];
    while let Some((c, nb)) = Command::decode(b) {
        ret.push(c);
        b = nb;
    }
    ret
}

fn encode_commands(cs: &[Command]) -> Vec<u8> {
    let mut ret = vec![];
    for c in cs.iter() {
        c.encode(&mut ret);
    }
    ret
}

#[derive(Debug)]
struct Model {
    r: usize,
    bd: Vec<Vec<Vec<bool>>>,
}

impl Model {
    fn decode(b: &[u8]) -> Model {
        let (r, b) = head(b);
        let r = r as usize;

        let mut bs = vec![];
        for r in b.iter() {
            for i in 0..8 {
                bs.push((*r & (1 << i)) != 0);
            }
        }

        let mut bd = vec![vec![vec![false; r]; r]; r];
        for i in 0..r {
            for j in 0..r {
                for k in 0..r {
                    bd[i][j][k] = bs[i * r * r + j * r + k];
                }
            }
        }

        Model { r, bd }
    }

    #[allow(dead_code)]
    fn print(&self) {
        println!("R = {}", self.r);

        for i in 0..self.r {
            for j in 0..self.r {
                for k in 0..self.r {
                    print!("{}", if self.bd[i][j][k] { 'X' } else { '.' });
                }
                println!();
            }
            println!();
        }
    }
}

#[derive(Debug)]
struct UnionFind {
    r: usize,
    parent: Vec<usize>,
}

impl UnionFind {
    fn new(r: usize, n: usize) -> UnionFind {
        UnionFind {
            r,
            parent: (0..n).collect(),
        }
    }

    fn find(&mut self, i: usize) -> usize {
        if self.parent[i] == i {
            return i;
        }

        let pi = self.parent[i];
        let p = self.find(pi);
        self.parent[i] = p;
        p
    }

    fn unite(&mut self, i: usize, j: usize) -> bool {
        let pi = self.find(i);
        let pj = self.find(j);
        if pi != pj {
            self.parent[pi] = pj;
            true
        } else {
            false
        }
    }

    fn unite_p(&mut self, x1: i32, y1: i32, z1: i32, x2: i32, y2: i32, z2: i32) -> bool {
        let i = self.enc(x1, y1, z1);
        let j = self.enc(x2, y2, z2);
        self.unite(i, j)
    }

    fn enc(&self, x: i32, y: i32, z: i32) -> usize {
        let r = self.r;
        x as usize * r * r + (y + 1) as usize * r + z as usize
    }
}

fn add_region(
    pos: &(i32, i32, i32),
    diff: &(i32, i32, i32),
    vols: &mut Vec<(i32, i32, i32)>,
    bd: &Vec<Vec<Vec<bool>>>,
) {
    assert!(
        (diff.0.abs() != 0 && diff.1.abs() == 0 && diff.2.abs() == 0)
            || (diff.0.abs() == 0 && diff.1.abs() != 0 && diff.2.abs() == 0)
            || (diff.0.abs() == 0 && diff.1.abs() == 0 && diff.2.abs() != 0)
    );

    {
        let mut t = *pos;
        t.0 += diff.0;
        t.1 += diff.1;
        t.2 += diff.2;
        assert!(
            t.0 >= 0
                && t.0 < bd.len() as i32
                && t.1 >= 0
                && t.1 < bd.len() as i32
                && t.2 >= 0
                && t.2 < bd.len() as i32,
            "Invalid move: {:?} -> {:?}",
            pos,
            diff,
        );
    }

    let mut pos = *pos;
    let mut diff = *diff;
    vols.push(pos);

    if diff.0.abs() > 0 {
        let d = if diff.0 > 0 { 1 } else { -1 };
        while diff.0 != 0 {
            pos.0 += d;
            diff.0 -= d;
            vols.push(pos);
            assert!(!bd[pos.0 as usize][pos.1 as usize][pos.2 as usize]);
        }
    } else if diff.1.abs() > 0 {
        let d = if diff.1 > 0 { 1 } else { -1 };
        while diff.1 != 0 {
            pos.1 += d;
            diff.1 -= d;
            vols.push(pos);
            assert!(!bd[pos.0 as usize][pos.1 as usize][pos.2 as usize]);
        }
    } else if diff.2.abs() > 0 {
        let d = if diff.2 > 0 { 1 } else { -1 };
        while diff.2 != 0 {
            pos.2 += d;
            diff.2 -= d;
            vols.push(pos);
            assert!(
                !bd[pos.0 as usize][pos.1 as usize][pos.2 as usize],
                "Move failed: {:?}",
                pos
            );
        }
    } else {
        unreachable!()
    }
}

const ADJACENT: &[Pos] = &[
    (0, 0, 1),
    (0, 0, -1),
    (0, 1, 0),
    (0, -1, 0),
    (1, 0, 0),
    (-1, 0, 0),
];

fn normalize_range(a: &Pos, b: &Pos) -> (Pos, Pos) {
    (
        (a.0.min(b.0), a.1.min(b.1), a.2.min(b.2)),
        (a.0.max(b.0), a.1.max(b.1), a.2.max(b.2)),
    )
}

struct State {
    strict: bool,
    r: usize,
    steps: usize,
    uf: UnionFind,
    islands: usize,

    energy: i64,
    harmonics: bool, // true if High, false if Low.
    matrix: Vec<Vec<Vec<bool>>>,
    bots: BTreeMap<usize, BotState>,
    trace: VecDeque<Command>,

    legalized_trace: Vec<Command>,
}

#[derive(Debug)]
struct BotState {
    bid: usize,
    pos: (i32, i32, i32),
    seeds: Vec<usize>,
}

impl State {
    fn new(
        r: usize,
        src_bd: Vec<Vec<Vec<bool>>>,
        trace: Vec<Command>,
        seeds: usize,
        strict: bool,
    ) -> State {
        let mut bots = BTreeMap::new();
        bots.insert(
            1,
            BotState {
                bid: 1,
                pos: (0, 0, 0),
                seeds: (2..=seeds).collect(),
            },
        );

        let mut uf = UnionFind::new(r, r * r * (r + 1));
        for x in 0..r as i32 - 1 {
            for z in 0..r as i32 - 1 {
                uf.unite_p(x, -1, z, x + 1, -1, z);
                uf.unite_p(x, -1, z, x, -1, z + 1);
            }
        }

        State {
            strict,
            r,
            steps: 0,
            uf,
            islands: 1,

            energy: 0,
            harmonics: false,
            matrix: src_bd,
            bots,
            trace: VecDeque::from(trace),

            legalized_trace: vec![],
        }
    }

    fn eval(&mut self) -> Result<()> {
        while self.bots.len() > 0 {
            // println!("{}", self.steps);
            self.step()?;
        }
        Ok(())
    }

    fn partial_eval(&mut self) -> Result<()> {
        while self.trace.len() > 0 && self.bots.len() > 0 {
            self.step()?;
        }
        Ok(())
    }

    fn step(&mut self) -> Result<()> {
        let num_bots = self.bots.len();
        let mut vols = vec![vec![]; num_bots];
        let mut added_bots = vec![];

        let mut halt = false;

        let mut fp = BTreeMap::<(Pos, Pos), usize>::new();
        let mut fs = BTreeMap::<(Pos, Pos), usize>::new();

        let mut gfill = BTreeMap::<(Pos, Pos), Vec<(Pos, Pos)>>::new();
        let mut gvoid = BTreeMap::<(Pos, Pos), Vec<(Pos, Pos)>>::new();

        for (_, bot) in self.bots.iter_mut() {
            assert!(bot.pos.0 >= 0 && bot.pos.0 < self.r as i32);
            assert!(bot.pos.1 >= 0 && bot.pos.1 < self.r as i32);
            assert!(bot.pos.2 >= 0 && bot.pos.2 < self.r as i32);
        }

        assert!(
            self.trace.len() >= self.bots.len(),
            "Command too short: bots = {:?}",
            self.bots
        );

        // FIXME: Voidが入ったときのGroundedチェックにはUnionFindは使えない

        for (bcnt, (bid, bot)) in self.bots.iter_mut().enumerate() {
            let cmd = self.trace.pop_front().unwrap();
            self.legalized_trace.push(cmd.clone());
            match cmd {
                Halt => {
                    vols[bcnt].push(bot.pos);

                    if bot.pos != (0, 0, 0) {
                        Err("Halt when bot is not on (0, 0, 0)")?;
                    }
                    if num_bots != 1 {
                        Err("Halt when S.bots != {bot}")?;
                    }
                    if self.harmonics != false {
                        Err("Halt when S.harmonics != Low")?;
                    }

                    halt = true;
                }

                Wait => {
                    vols[bcnt].push(bot.pos);
                }

                Flip => {
                    vols[bcnt].push(bot.pos);
                    self.harmonics = !self.harmonics;
                }

                SMove(lld) => {
                    add_region(&bot.pos, &lld.pos(), &mut vols[bcnt], &self.matrix);
                    bot.pos += lld;
                    self.energy += 2 * lld.mlen();
                }

                LMove(sld1, sld2) => {
                    add_region(&bot.pos, &sld1.pos(), &mut vols[bcnt], &self.matrix);
                    bot.pos += sld1.clone();
                    add_region(&bot.pos, &sld2.pos(), &mut vols[bcnt], &self.matrix);
                    bot.pos += sld2.clone();
                    self.energy += 2 * (sld1.mlen() + 2 + sld2.mlen());
                }

                Fission(nd, m) => {
                    assert!(
                        m < bot.seeds.len(),
                        "Fission: not enough seeds: seeds={:?}, m={}",
                        bot.seeds,
                        m
                    );

                    vols[bcnt].push(bot.pos);
                    let t = bot.pos + nd;
                    vols[bcnt].push(t);

                    added_bots.push(BotState {
                        bid: bot.seeds[0],
                        pos: t,
                        seeds: bot.seeds[1..m + 1].to_owned(),
                    });
                    bot.seeds = bot.seeds[m + 1..].to_owned();
                    self.energy += 24;
                }

                Fill(nd) => {
                    let t = bot.pos + nd;
                    vols[bcnt].push(bot.pos);
                    vols[bcnt].push(t);

                    if !self.matrix[t.0 as usize][t.1 as usize][t.2 as usize] {
                        self.matrix[t.0 as usize][t.1 as usize][t.2 as usize] = true;

                        self.energy += 12;

                        if self.strict {
                            self.islands += 1;

                            for (dx, dy, dz) in ADJACENT.iter() {
                                let nx = t.0 + dx;
                                let ny = t.1 + dy;
                                let nz = t.2 + dz;
                                if !(nx >= 0
                                    && nx < self.r as i32
                                    && ny >= -1
                                    && ny < self.r as i32
                                    && nz >= 0
                                    && nz < self.r as i32)
                                {
                                    continue;
                                }
                                if ny >= 0 && !self.matrix[nx as usize][ny as usize][nz as usize] {
                                    continue;
                                }

                                if self.uf.unite_p(t.0, t.1, t.2, nx, ny, nz) {
                                    self.islands -= 1;
                                }
                            }
                        }
                    } else {
                        self.energy += 6;
                    }
                }

                Void(nd) => {
                    let t = bot.pos + nd;
                    vols[bcnt].push(bot.pos);
                    vols[bcnt].push(t);

                    let mut r = &mut self.matrix[t.0 as usize][t.1 as usize][t.2 as usize];
                    if *r {
                        self.energy -= 12;
                    } else {
                        self.energy += 3;
                    }
                    *r = false;
                }

                GFill(nd, fd) => {
                    let a = bot.pos + nd;
                    let b = a + fd;
                    let r = normalize_range(&a, &b);
                    gfill
                        .entry(r)
                        .or_insert(Default::default())
                        .push((a, bot.pos));
                }

                GVoid(nd, fd) => {
                    let a = bot.pos + nd;
                    let b = a + fd;
                    let r = normalize_range(&a, &b);
                    gvoid
                        .entry(r)
                        .or_insert(Default::default())
                        .push((a, bot.pos));
                }

                FusionP(nd) => {
                    vols[bcnt].push(bot.pos);
                    fp.insert((bot.pos, bot.pos + nd), *bid);
                }

                FusionS(nd) => {
                    vols[bcnt].push(bot.pos);
                    fs.insert((bot.pos + nd, bot.pos), *bid);
                }

                _ => unreachable!(),
            }
        }

        if halt {
            self.bots.clear();
        }

        for b in added_bots {
            self.bots.insert(b.bid, b);
        }

        for (key, bid) in fp {
            if !fs.contains_key(&key) {
                Err(format!(
                    "FusionP is not paired[{}]: {:?}, {}",
                    self.steps, key, bid
                ))?;
            }

            let cid = fs[&key];
            let mut cids = self.bots.remove(&cid).unwrap();
            self.bots
                .get_mut(&bid)
                .unwrap()
                .seeds
                .append(&mut cids.seeds);
            self.bots.get_mut(&bid).unwrap().seeds.sort();
            fs.remove(&key);

            self.energy -= 24;
        }

        if !fs.is_empty() {
            Err(format!("FusionS is not paired: {:?}", fs))?;
        }

        // GFill/GVoid operation
        let mut do_group_op = false;
        for (data, opname, isfill) in &[(gfill, "GFill", true), (gvoid, "GVoid", false)] {
            for ((r, s), member) in data.iter() {
                let (verts, mut botpos): (Vec<Pos>, Vec<Pos>) = member.iter().cloned().unzip();

                assert!(
                    r.0 >= 0
                        && r.1 >= 0
                        && r.2 >= 0
                        && s.0 < self.r as i32
                        && s.1 < self.r as i32
                        && s.2 < self.r as i32,
                    "{}: out of range {:?}",
                    opname,
                    (r, s)
                );

                let dim = (r.0 != s.0) as u32 + (r.1 != s.1) as u32 + (r.2 != s.2) as u32;

                assert!(
                    member.len() == 2usize.pow(dim),
                    "Not enough member[{}]: {} {:?} {}=[{:?}]",
                    self.steps,
                    opname,
                    r,
                    member.len(),
                    member,
                );

                for b in 0..8 {
                    let p = (
                        if b & 1 == 0 {
                            r.0
                        } else {
                            s.0
                        },
                        if b & 1 == 0 {
                            r.1
                        } else {
                            s.1
                        },
                        if b & 1 == 0 {
                            r.2
                        } else {
                            s.2
                        },
                    );

                    if !verts.contains(&p) {
                        Err(format!(
                            "Not enough member: {} {:?} {:?}",
                            opname, r, member
                        ))?;
                    }
                }

                // if self.strict {
                //     println!("*** EXEC {}: [{:?}, {:?}]", opname, r, s);
                // }

                let mut vola = vec![];

                for x in r.0..=s.0 {
                    for y in r.1..=s.1 {
                        for z in r.2..=s.2 {
                            let r = &mut self.matrix[x as usize][y as usize][z as usize];

                            if *isfill {
                                if !*r {
                                    *r = true;
                                    self.energy += 12;
                                } else {
                                    self.energy += 6;
                                }
                            } else {
                                if *r {
                                    *r = false;
                                    self.energy -= 12;
                                } else {
                                    self.energy += 3;
                                }
                            }
                            vola.push((x, y, z));
                        }
                    }
                }
                vola.append(&mut botpos);
                vols.push(vola);
                do_group_op = true;

                let empty = self.matrix
                    .iter()
                    .all(|plane| plane.iter().all(|row| row.iter().all(|b| !*b)));

                if empty {
                    // UFを使い始める
                    // println!("Enter strict mode");
                    self.strict = true;
                    assert!(self.islands == 1);
                }
            }
        }

        self.energy += 20 * self.bots.len().max(1) as i64;

        if self.harmonics {
            self.energy += 30 * self.r.pow(3) as i64;
        } else {
            self.energy += 3 * self.r.pow(3) as i64;
        }

        self.steps += 1;

        // TODO: ハーモニクスチェック
        // TODO: Groundedかどうかをどうやって調べる？
        if !self.harmonics {
            // check if grounded
            if do_group_op && !self.check_grounded_bfs() {
                Err("Harmonics is Low and not granded")?;
            }
        }

        if self.strict {
            if self.harmonics == false && self.islands > 1 {
                Err(format!(
                    "Harmonics is Low and not granded: {}",
                    self.islands
                ))?;
            }
        }

        /*
        if self.strict {
            if self.harmonics == false && self.islands > 1 {
                panic!("Harmonics is Low and not granded: {}", self.islands);
            }
        } else {
            if (self.harmonics == false && self.islands > 1)
                || (self.harmonics == true && self.islands == 1)
            {
                let mut done = false;
                assert!(self.legalized_trace.len() >= self.bots.len());
                for i in 0..self.bots.len() {
                    let ix = self.legalized_trace.len() - 1 - i;
                    if self.legalized_trace[ix] == Wait {
                        self.legalized_trace[ix] = Flip;
                        done = true;
                        self.harmonics = !self.harmonics;
                        break;
                    }
                }

                // TODO: ハーモニクスオフの時も無理矢理やったほうがいい？
                if !done && self.harmonics == false && self.islands > 1 {
                    for i in 0..self.bots.len() {
                        self.legalized_trace.push(if i == 0 { Flip } else { Wait });
                    }

                    for i in 0..self.bots.len() {
                        let ix = self.legalized_trace.len() - 1 - i;
                        self.legalized_trace.swap(ix, ix - self.bots.len());
                    }
                    self.harmonics = !self.harmonics;
                }
            }
        }
*/

        for i in 0..vols.len() {
            for j in i + 1..vols.len() {
                for k in 0..vols[i].len() {
                    for l in 0..vols[j].len() {
                        if vols[i][k] == vols[j][l] {
                            Err(format!(
                                "Multiple volatile coordinates[{}]: {:?}",
                                self.steps, vols[i][k]
                            ))?;
                        }
                    }
                }
            }
        }

        // これいる？
        // for (_, bot) in self.bots.iter() {
        //     let p = bot.pos;
        //     if self.matrix[p.0 as usize][p.1 as usize][p.2 as usize] {
        //         panic!("Bot in not Void position: {:?}", p);
        //     }
        // }

        Ok(())
    }

    fn check_grounded_bfs(&self) -> bool {
        // voidしないモードなら
        if self.strict {
            return self.islands == 1;
        }

        let mut m = self.matrix.clone();
        let r = self.r as i32;
        let mut q = VecDeque::new();

        for x in 0..r {
            for z in 0..r {
                if m[x as usize][0][z as usize] {
                    q.push_back((x, 0, z));
                    m[x as usize][0][z as usize] = false;
                }
            }
        }

        while let Some((x, y, z)) = q.pop_front() {
            for (dx, dy, dz) in ADJACENT.iter() {
                let cx = x + dx;
                let cy = y + dy;
                let cz = z + dz;

                if !(cx >= 0 && cx < r && cy >= 0 && cy < r && cz >= 0 && cz < r) {
                    continue;
                }

                if m[cx as usize][cy as usize][cz as usize] {
                    q.push_back((cx, cy, cz));
                    m[cx as usize][cy as usize][cz as usize] = false;
                }
            }
        }

        for x in 0..r {
            for y in 0..r {
                for z in 0..r {
                    if m[x as usize][y as usize][z as usize] {
                        return false;
                    }
                }
            }
        }

        true
    }
}

fn eval_lightning(name: &str, model: &Model, trace: &[Command]) -> Result<()> {
    let state = eval(None, Some(model), trace, 20)?;

    let bs = get_best_score(LIGHTNING_DIR, name).unwrap();

    println!(
        "Final state for {}: energy = {:16} ({:16}, {:4.03}%), steps = {:8}, R = {:3}",
        name,
        state.energy,
        bs.unwrap_or(0),
        if let Some(bs) = bs {
            bs as f64 / state.energy as f64 * 100.0
        } else {
            0.0
        },
        state.steps,
        state.r
    );

    save_solution(LIGHTNING_DIR, name, &trace, state.energy)
}

fn eval_full(
    name: &str,
    src: Option<&Model>,
    tgt: Option<&Model>,
    trace: &[Command],
) -> Result<()> {
    let state = eval(src, tgt, trace, 40)?;
    let bs = get_best_score(FULL_DIR, name)?;

    println!(
        "Final state for {}: energy = {:16} ({:16}, {:4.03}%), steps = {:8}, R = {:3}",
        name,
        state.energy,
        bs.unwrap_or(0),
        if let Some(bs) = bs {
            bs as f64 / state.energy as f64 * 100.0
        } else {
            0.0
        },
        state.steps,
        state.r
    );

    save_solution(FULL_DIR, name, &trace, state.energy)
}

fn eval(
    src: Option<&Model>,
    tgt: Option<&Model>,
    trace: &[Command],
    seeds: usize,
) -> Result<State> {
    let r = src.or(tgt).unwrap().r;

    let src_bd = if let Some(p) = src {
        p.bd.clone()
    } else {
        vec![vec![vec![false; r]; r]; r]
    };

    let tgt_bd = if let Some(p) = tgt {
        p.bd.clone()
    } else {
        vec![vec![vec![false; r]; r]; r]
    };

    let mut state = State::new(
        r,
        src_bd,
        trace.to_vec(),
        seeds,
        // Rモードの時はUF使えない
        if src.is_some() && tgt.is_some() {
            false
        } else {
            true
        },
    );
    state.eval()?;

    if state.harmonics != false {
        Err(format!("Final harmonics is not Low"))?;
    }
    if tgt_bd != state.matrix {
        Err(format!("Final matrix is not equal to target"))?;
    }
    if state.bots.len() != 0 {
        Err(format!("Bot remains"))?;
    }
    if state.trace.len() != 0 {
        Err(format!("Comand remains"))?;
    }

    Ok(state)
}

//----------
// Solution

struct KDTree {
    sum: usize,
    data: Vec<Vec<usize>>,
    offset: (usize, usize),
    child: Vec<KDTree>,
}

impl KDTree {
    fn new(offset: (usize, usize), data: &Vec<Vec<usize>>) -> KDTree {
        KDTree {
            sum: data.iter().map(|r| r.iter().sum::<usize>()).sum(),
            data: data.clone(),
            offset,
            child: vec![],
        }
    }

    fn ranges(&self) -> Vec<(usize, (usize, usize), (usize, usize))> {
        if self.child.len() == 0 {
            vec![(self.sum, self.offset, (self.data.len(), self.data[0].len()))]
        } else {
            let mut l = self.child[0].ranges();
            let mut r = self.child[1].ranges();
            l.append(&mut r);
            l
        }
    }

    fn max_leaf(&mut self) -> (usize, (usize, usize)) {
        if self.child.len() == 0 {
            if self.data.len() == 1 && self.data[0].len() == 1 {
                return (0, self.offset);
            }
            return (self.sum, self.offset);
        }

        assert!(self.child.len() == 2, "child.len() = {}", self.child.len());

        let l = self.child[0].max_leaf();
        let r = self.child[1].max_leaf();

        if l.0 >= r.0 {
            l
        } else {
            r
        }
    }

    fn split(&mut self, offset: (usize, usize)) {
        if self.offset != offset {
            if self.child.len() > 0 {
                self.child[0].split(offset);
                self.child[1].split(offset);
            }
            return;
        }

        assert!(self.child.len() == 0);

        let w = self.data.len();
        let h = self.data[0].len();

        assert!(w >= 2 || h >= 2);

        let mut hist_x = vec![0; w];
        let mut hist_y = vec![0; h];

        for x in 0..w {
            for y in 0..h {
                hist_x[x] += self.data[x][y];
                hist_y[y] += self.data[x][y];
            }
        }

        let mut best_score = 100.0;
        let mut best_axis = (0, 0);

        {
            let mut acc = 0;
            for x in 0..w - 1 {
                acc += hist_x[x];

                let s = (acc as f64 / self.sum as f64 - 0.5).abs();
                if s < best_score {
                    best_score = s;
                    best_axis = (0, x + 1)
                }
            }
        }

        {
            let mut acc = 0;
            for y in 0..h - 1 {
                acc += hist_y[y];

                let s = (acc as f64 / self.sum as f64 - 0.5).abs();
                if s < best_score {
                    best_score = s;
                    best_axis = (1, y + 1)
                }
            }
        }

        if best_axis.0 == 0 {
            let mut l = vec![vec![0; h]; best_axis.1];
            let mut r = vec![vec![0; h]; w - best_axis.1];

            for x in 0..w {
                for y in 0..h {
                    if x < best_axis.1 {
                        l[x][y] = self.data[x][y];
                    } else {
                        r[x - best_axis.1][y] = self.data[x][y];
                    }
                }
            }

            let l = KDTree::new(self.offset, &l);
            let r = KDTree::new((self.offset.0 + best_axis.1, self.offset.1), &r);

            self.child.push(l);
            self.child.push(r);
        } else {
            let mut l = vec![vec![0; best_axis.1]; w];
            let mut r = vec![vec![0; h - best_axis.1]; w];

            for x in 0..w {
                for y in 0..h {
                    if y < best_axis.1 {
                        l[x][y] = self.data[x][y];
                    } else {
                        r[x][y - best_axis.1] = self.data[x][y];
                    }
                }
            }

            let l = KDTree::new(self.offset, &l);
            let r = KDTree::new((self.offset.0, self.offset.1 + best_axis.1), &r);

            self.child.push(l);
            self.child.push(r);
        }

        self.offset = (99999, 99999);
    }
}

fn solve(model: &Model, axis: u32, disharmo: u32) -> Vec<Command> {
    let r = model.r;

    let mut ret = vec![];

    let max_worker = 20;

    // let axis = if axis { 1 } else { 0 };
    // let axis = 2;

    // load balancing
    let ranges = if axis == 2 {
        // KDTreeによるロードバランス
        let mut hist_xz = vec![vec![0; r]; r];
        for x in 0..r {
            for y in 0..r {
                for z in 0..r {
                    if model.bd[x][y][z] {
                        hist_xz[x][z] += 1;
                    }
                }
            }
        }

        let mut kdt = KDTree::new((0, 0), &hist_xz);
        for _ in 0..max_worker - 1 {
            let (_, ofs) = kdt.max_leaf();
            kdt.split(ofs);
        }

        let mut ranges = kdt.ranges();
        ranges.sort_by(|r, s| r.1.cmp(&s.1));

        ranges
    } else if axis == 0 {
        // x軸分割によるロードバランス
        let mut hist_x = vec![0i64; r];

        for x in 0..r {
            for y in 0..r {
                for z in 0..r {
                    if model.bd[x][y][z] {
                        hist_x[x] += 1;
                    }
                }
            }
        }

        let total: i64 = hist_x.iter().sum();
        let mut xs = vec![];
        let mut cx = 0;
        let mut sum = 0;
        xs.push(0);
        while cx < hist_x.len() && sum < total {
            let ix = xs.len() as i64;
            let lim = total * ix as i64 / max_worker;
            sum += hist_x[cx];
            cx += 1;

            while cx < hist_x.len() && sum < lim {
                if sum + hist_x[cx] >= lim && (sum + hist_x[cx] - lim).abs() > (sum - lim).abs() {
                    break;
                }
                sum += hist_x[cx];
                cx += 1;
            }
            xs.push(cx);
        }

        assert!(xs.len() <= max_worker as usize + 1);

        let mut ranges = vec![];
        for i in 0..xs.len() - 1 {
            ranges.push((
                hist_x[xs[i]..xs[i + 1]].iter().cloned().sum::<i64>() as usize,
                (xs[i], 0),
                (xs[i + 1] - xs[i], r),
            ));
        }
        ranges
    } else {
        // z軸分割によるロードバランス
        let mut hist_x = vec![0i64; r];

        for x in 0..r {
            for y in 0..r {
                for z in 0..r {
                    if model.bd[x][y][z] {
                        hist_x[z] += 1;
                    }
                }
            }
        }

        let total: i64 = hist_x.iter().sum();
        let mut xs = vec![];
        let mut cx = 0;
        let mut sum = 0;
        xs.push(0);
        while cx < hist_x.len() && sum < total {
            let ix = xs.len() as i64;
            let lim = total * ix as i64 / max_worker;
            sum += hist_x[cx];
            cx += 1;

            while cx < hist_x.len() && sum < lim {
                if sum + hist_x[cx] >= lim && (sum + hist_x[cx] - lim).abs() > (sum - lim).abs() {
                    break;
                }
                sum += hist_x[cx];
                cx += 1;
            }
            xs.push(cx);
        }

        // println!("xs: {:?}", xs);

        assert!(xs.len() <= max_worker as usize + 1);

        let mut ranges = vec![];
        for i in 0..xs.len() - 1 {
            ranges.push((
                hist_x[xs[i]..xs[i + 1]].iter().cloned().sum::<i64>() as usize,
                (0, xs[i]),
                (r, xs[i + 1] - xs[i]),
            ));
        }
        ranges
    };

    // println!("Ranges: {:?}", ranges);

    // ワーカー
    let mut ss = vec![vec![]; ranges.len()];

    // 必要ないワーカーは作らない
    for i in 0..ss.len() - 1 {
        for _ in 0..i {
            ret.push(Wait);
        }

        if axis != 1 {
            ret.push(Fission(ND::new(1, 0, 0), ss.len() - 2 - i));
        } else {
            ret.push(Fission(ND::new(0, 0, 1), ss.len() - 2 - i));
        }
    }

    // ハーモニクス操作スレッド
    // {
    //     let tid = (ss.len() - 1) as i32;
    //     let init = (0, 0, tid);
    //     let trace = &mut ss[tid as usize];
    //     let mut cur = init.clone();
    //     move_to(&mut cur, &(0, 0, model.r as i32 - 1), trace);
    //     trace.push(Barrier);

    //     for j in 0..=tid as i32 {
    //         if tid == j {
    //             move_to(&mut cur, &init, trace);
    //             trace.push(Barrier);
    //         // trace.push(FusionS(ND(-1, 0, 0)));
    //         // break;
    //         } else {
    //             trace.push(Barrier);
    //         }
    //     }
    //     // trace.push(Barrier);
    // }

    // println!("Harmonics thread: {:?}", ss[ss.len() - 1]);

    let maxr = ranges.iter().map(|r| r.0).max().unwrap_or(1);

    let nworker = ss.len();
    assert!(
        ranges.len() == nworker,
        "ranges: {}, nworker: {}",
        ranges.len(),
        nworker
    );

    for i in 0..nworker {
        // let lx = xs[i] as i32;
        // let ux = xs[i + 1] as i32;

        let lx = (ranges[i].1).0 as i32;
        let ux = (ranges[i].1).0 as i32 + (ranges[i].2).0 as i32;

        let lz = (ranges[i].1).1 as i32;
        let uz = (ranges[i].1).1 as i32 + (ranges[i].2).1 as i32;

        let prob = ranges[i].0 as f64 / maxr as f64;

        let mut trace = vec![];
        let mut cur = if axis != 1 {
            (i as i32, 0, 0)
        } else {
            (0, 0, i as i32)
        };

        // 引っかからないようにするため
        trace.push(Wait);
        for _ in 0..(nworker - 1 - i) {
            trace.push(Wait);
            // 引っかかるときがある

            if axis == 2 {
                trace.push(Wait);
            }
        }
        if axis == 2 {
            move_to(&mut cur, &(lx, 1, lz), &mut trace);
        }

        let mut ord = vec![];
        if axis != 1 {
            for y in 0..r as i32 {
                let xs: Vec<i32> = if y % 2 == 0 {
                    (lx..ux).collect()
                } else {
                    (lx..ux).rev().collect()
                };

                for x in xs {
                    let zs: Vec<i32> = if (x - lx + y) % 2 == 0 {
                        (lz..uz).collect()
                    } else {
                        (lz..uz).rev().collect()
                    };

                    for z in zs {
                        ord.push((x as i32, y as i32, z as i32));
                    }
                }
            }
        } else {
            for y in 0..r as i32 {
                let zs: Vec<i32> = if y % 2 == 0 {
                    (lz..uz).collect()
                } else {
                    (lz..uz).rev().collect()
                };

                for z in zs {
                    let xs: Vec<i32> = if (z - lz + y) % 2 == 0 {
                        (lx..ux).collect()
                    } else {
                        (lx..ux).rev().collect()
                    };

                    for x in xs {
                        ord.push((x as i32, y as i32, z as i32));
                    }
                }
            }
        }

        let mut oi = 0;

        loop {
            // assert!(cur.0 >= lx && cur.0 < ux && cur.2 >= lz && cur.2 < uz);

            if trace.len() > 22 && (1.0 - prob) / 1.2 > rand::random() {
                trace.push(Wait);
                // continue;
            }

            let mut next = None;
            while oi < ord.len() {
                let t = ord[oi];
                oi += 1;
                if model.bd[t.0 as usize][t.1 as usize][t.2 as usize] {
                    next = Some(t);
                    break;
                }
            }

            if next == None {
                // z -> y -> barrier x
                let t = if axis != 1 {
                    (i as i32, 0, 0)
                } else {
                    (0, 0, i as i32)
                };
                // let t = if i == 0 { (0, 0, 0) } else { (1, 0, 0) };
                // let c = cur.clone();
                // move_to(&mut cur, &(lx, r as i32 - 1, lz), &mut trace);
                let c = cur.clone();

                if axis == 0 {
                    move_to(&mut cur, &(c.0, c.1, t.2), &mut trace);
                    move_to(&mut cur, &(c.0, t.1, t.2), &mut trace);
                } else if axis == 1 {
                    move_to(&mut cur, &(t.0, c.1, c.2), &mut trace);
                    move_to(&mut cur, &(t.0, t.1, c.2), &mut trace);
                }
                trace.push(Barrier);

                for j in 0..ss.len() {
                    if i == j {
                        if axis == 2 {
                            move_to(&mut cur, &(c.0, r as i32 - 1, c.2), &mut trace);
                            move_to(&mut cur, &(c.0, r as i32 - 1, 0), &mut trace);
                            move_to(&mut cur, &(t.0, r as i32 - 1, 0), &mut trace);
                            move_to(&mut cur, &(t.0, 0, 0), &mut trace);
                        }

                        move_to(&mut cur, &(t.0, t.1, t.2), &mut trace);
                        trace.push(Barrier);
                    } else {
                        trace.push(Barrier);
                    }
                }

                // trace.push(Barrier);
                break;
            }

            let next = next.unwrap();

            if next.1 == cur.1 - 1 && (next.0 - cur.0).abs() + (next.2 - cur.2).abs() <= 1 {
                trace.push(Fill(ND::new(next.0 - cur.0, -1, next.2 - cur.2)));
            } else {
                if axis == 0 {
                    let dz = if (next.0 - lx + next.1) % 2 == 0 {
                        1
                    } else {
                        -1
                    };
                    move_to(&mut cur, &(next.0, next.1 + 1, next.2 + dz), &mut trace);
                    trace.push(Fill(ND::new(0, -1, -dz)));
                } else if axis == 1 {
                    let dx = if (next.2 - lz + next.1) % 2 == 0 {
                        1
                    } else {
                        -1
                    };
                    move_to(&mut cur, &(next.0 + dx, next.1 + 1, next.2), &mut trace);
                    trace.push(Fill(ND::new(-dx, -1, 0)));
                } else {
                    move_to(&mut cur, &(next.0, next.1 + 1, next.2), &mut trace);
                    trace.push(Fill(ND::new(0, -1, 0)));
                }
            }
        }

        ss[i as usize] = trace;
    }

    if disharmo == 0 {
        let mut tt = normal_merge(&ss);
        ret.append(&mut tt);
    } else {
        // ハーモニクス抑制
        // let mut state = State::new(r, ret.clone(), false);
        // let tt = dis_harmonics(&mut state, &ss, disharmo);
        // if tt.is_none() {
        //     return vec![];
        // } else {
        //     let mut tt = tt.unwrap();
        //     ret.append(&mut tt);
        // }
    }

    for i in 0..ss.len() - 1 {
        for _ in 0..ss.len() - 2 - i {
            ret.push(Wait);
        }

        if axis != 1 {
            ret.push(FusionP(ND::new(1, 0, 0)));
            ret.push(FusionS(ND::new(-1, 0, 0)));
        } else {
            ret.push(FusionP(ND::new(0, 0, 1)));
            ret.push(FusionS(ND::new(0, 0, -1)));
        }
    }

    ret.push(Halt);

    // {
    //     let mut state = State::new(model.r, ret.to_vec(), false);
    //     state.eval();
    //     state.legalized_trace.clone()
    // }

    ret
}

fn normal_merge(ths: &Vec<Vec<Command>>) -> Vec<Command> {
    let mut ret = vec![];

    let mut ixs = vec![0; ths.len()];

    loop {
        let mut bcnt = 0;
        let mut ends = 0;

        for i in 0..ths.len() {
            if ixs[i] >= ths[i].len() {
                ends += 1;
                // bcnt += 1;
                continue;
            }

            if ths[i][ixs[i]] == Barrier {
                ret.push(Wait);
                bcnt += 1;
                continue;
            }

            ret.push(ths[i][ixs[i]].clone());
            // if let FusionS(_) = ths[i][ixs[i]] {
            //     live -= 1;
            //     ixs[i] = 99999999;
            // } else {
            ixs[i] += 1;
            // }
        }

        if bcnt == ths.len() {
            // ラスト ths.len()は全部 wait だから消してもいい
            // FusionPでスレッドなくなるからそうとも言えない

            for i in 0..ths.len() {
                ixs[i] += 1;
                ret.pop();
            }
        }

        if ends == 0 {
            continue;
        }

        if ends == ths.len() {
            break;
        }

        // 長さがあってない
        println!(
            "{}, {}, {:?}, {:?}",
            ends,
            ths.len(),
            ixs,
            ths.iter().map(|s| s.len()).collect::<Vec<_>>()
        );
        unreachable!();
    }

    ret
}

fn dis_harmonics(
    state: &mut State,
    ths: &Vec<Vec<Command>>,
    init_wait: u32,
) -> Option<Vec<Command>> {
    let mut ret = vec![];

    let mut ixs = vec![0; ths.len()];
    let mut waits = vec![0; ths.len()];

    let mut wait_limit = init_wait;

    for _ in 0..100000 {
        state.partial_eval();

        let mut bcnt = 0;
        let mut ends = 0;

        for i in 0..ths.len() {
            if ixs[i] >= ths[i].len() {
                ends += 1;
                // bcnt += 1;
                continue;
            }

            if ths[i][ixs[i]] == Barrier {
                ret.push(Wait);
                state.trace.push_back(Wait);
                bcnt += 1;
                continue;
            }

            if let Fill(nd) = &ths[i][ixs[i]] {
                let mut t = state.bots.iter().nth(i).unwrap().1.pos;
                t += nd.clone();

                let mut ok = false;

                // 床からたどれるところと繋がっていない所はFillせずに待ってみる
                for (dx, dy, dz) in [
                    (0, 0, 1),
                    (0, 0, -1),
                    (0, 1, 0),
                    (0, -1, 0),
                    (1, 0, 0),
                    (-1, 0, 0),
                ].iter()
                {
                    let nx = t.0 + dx;
                    let ny = t.1 + dy;
                    let nz = t.2 + dz;

                    if nx >= 0
                        && nx < state.r as i32
                        && ny >= -1
                        && ny < state.r as i32
                        && nz >= 0
                        && nz < state.r as i32
                        && (ny < 0 || state.matrix[nx as usize][ny as usize][nz as usize])
                    {
                        ok = true;
                    }
                }

                if ok || waits[i] >= wait_limit - 1 {
                    ret.push(Fill(nd.clone()));
                    state.trace.push_back(Fill(nd.clone()));
                    ixs[i] += 1;
                    waits[i] = 0;
                    if !ok {
                        wait_limit = (wait_limit / 2).max(1);
                    } else {
                        wait_limit = (wait_limit * 2).min(init_wait);
                    }
                } else {
                    ret.push(Wait);
                    state.trace.push_back(Wait);
                    waits[i] += 1;
                }

                continue;
            }

            ret.push(ths[i][ixs[i]].clone());
            state.trace.push_back(ths[i][ixs[i]].clone());
            ixs[i] += 1;
        }

        if bcnt == ths.len() {
            // ラスト ths.len()は全部 wait だから消してもいい
            // FusionPでスレッドなくなるからそうとも言えない

            for i in 0..ths.len() {
                ixs[i] += 1;
                ret.pop();
            }
        }

        if ends == 0 {
            continue;
        }

        if ends == ths.len() {
            return Some(ret);
        }

        // 長さがあってない
        println!(
            "{}, {}, {:?}, {:?}",
            ends,
            ths.len(),
            ixs,
            ths.iter().map(|s| s.len()).collect::<Vec<_>>()
        );
        unreachable!();
    }

    // panic!("Disharmonics failed");
    None
}

fn move_to(from: &mut Pos, to: &Pos, cmds: &mut Vec<Command>) {
    if from == to {
        return;
    }

    {
        let v = vect(from, to);
        if v.0.abs() <= 15 && v.1.abs() <= 15 && v.2.abs() <= 15 {
            if v.0 == 0 && v.1 == 0 {
                cmds.push(SMove(LLD::new(0, 0, v.2)));
                *from = *to;
                return;
            }
            if v.1 == 0 && v.2 == 0 {
                cmds.push(SMove(LLD::new(v.0, 0, 0)));
                *from = *to;
                return;
            }
            if v.2 == 0 && v.0 == 0 {
                cmds.push(SMove(LLD::new(0, v.1, 0)));
                *from = *to;
                return;
            }
        }
    }

    {
        let v = vect(from, to);
        if v.0.abs() <= 5 && v.1.abs() <= 5 && v.2.abs() <= 5 {
            if v.0 == 0 {
                assert!(v.1 != 0 && v.2 != 0);
                cmds.push(LMove(SLD::new(0, v.1, 0), SLD::new(0, 0, v.2)));
                *from = *to;
                return;
            }
            if v.1 == 0 {
                assert!(v.2 != 0 && v.0 != 0);
                cmds.push(LMove(SLD::new(v.0, 0, 0), SLD::new(0, 0, v.2)));
                *from = *to;
                return;
            }
            if v.2 == 0 {
                assert!(v.0 != 0 && v.1 != 0);
                cmds.push(LMove(SLD::new(v.0, 0, 0), SLD::new(0, v.1, 0)));
                *from = *to;
                return;
            }
        }
    }

    while from != to {
        // println!("{:?}", v);
        let v = vect(from, to);

        if v.1 != 0 {
            let d = v.1.min(15).max(-15);
            cmds.push(SMove(LLD::new(0, d, 0)));
            from.1 += d;
            continue;
        }

        if v.2 != 0 {
            let d = v.2.min(15).max(-15);
            cmds.push(SMove(LLD::new(0, 0, d)));
            from.2 += d;
            continue;
        }

        if v.0 != 0 {
            let d = v.0.min(15).max(-15);
            cmds.push(SMove(LLD::new(d, 0, 0)));
            from.0 += d;
            continue;
        }
    }
}

// struct Resource {
//     state:
// }

struct Manager {
    trace: Vec<Command>,
    recv: Vec<Receiver<(Pos, Command)>>,
    buf: Vec<Option<(Pos, Command)>>,
    ack: Vec<Sender<()>>,
    state: Arc<Mutex<State>>,

    dpos: Vec<(Pos)>, // 本来いる位置からの差分
}

impl Manager {
    fn new(state: Arc<Mutex<State>>) -> Manager {
        Manager {
            trace: vec![],
            recv: vec![],
            buf: vec![],
            ack: vec![],
            state,
            dpos: vec![],
        }
    }

    fn add_builder(&mut self, recv: Receiver<(Pos, Command)>, ack: Sender<()>) {
        self.recv.push(recv);
        self.buf.push(None);
        self.ack.push(ack);
        self.dpos.push((0, 0, 0));
    }

    fn push(&mut self, c: Command) {
        self.trace.push(c.clone());
        self.state.lock().unwrap().trace.push_back(c);
    }

    fn insert_flip(&mut self, swap: bool) {
        let ths = self.recv.len();

        let h = self.state.lock().unwrap().harmonics;
        self.state.lock().unwrap().harmonics = !h;

        // Waitがあればねじ込む
        let n = self.trace.len();
        if n >= ths {
            for i in 0..ths {
                if self.trace[n - 1 - i] == Wait {
                    self.trace[n - 1 - i] = Flip;
                    return;
                }
            }
        }

        // なければ前のターンか次のターンにFlipしてたことにする
        for i in 0..ths {
            self.trace.push(if i == 0 { Flip } else { Wait });
        }
        if swap {
            for i in 0..ths {
                let n = self.trace.len();
                self.trace.swap(n - 1 - i, n - 1 - i - ths);
            }
        }
    }

    fn start(init_axis: usize, mng: Arc<Mutex<Manager>>) -> JoinHandle<()> {
        thread::spawn(move || {
            let fdir = match init_axis {
                0 => (1, 0, 0),
                1 => (0, 1, 0),
                2 => (0, 0, 1),
                _ => unreachable!(),
            };

            // Prologue code
            {
                let mut mng = mng.lock().unwrap();
                let n = mng.recv.len();
                const SEED_NUM: usize = 40;

                for i in 0..n - 1 {
                    for _ in 0..i {
                        mng.push(Wait);
                    }
                    mng.push(Fission(ND::from_pos(fdir.clone()), SEED_NUM - 2 - i));
                }
            }

            let mut updated = true;

            loop {
                let mut mng = mng.lock().unwrap();

                let err = mng.state.lock().unwrap().partial_eval();

                if err.is_err() {
                    if !mng.state.lock().unwrap().check_grounded_bfs() {
                        // println!(
                        //     "* harmonics change: High, ground check: {}, {:?}",
                        //     mng.state.lock().unwrap().check_grounded_bfs(),
                        //     err
                        // );
                        mng.insert_flip(true);
                    } else {
                        unreachable!("What error???: {:?}", err);
                    }
                } else if mng.state.lock().unwrap().harmonics {
                    if mng.state.lock().unwrap().check_grounded_bfs() {
                        // println!(
                        //     "* harmonics change: Low, ground check: {}",
                        //     mng.state.lock().unwrap().check_grounded_bfs()
                        // );
                        mng.insert_flip(false);
                    }
                }

                /*
                assert!(
                    mng.state.lock().unwrap().steps < 200000,
                    "Too long: step={}, {:?}",
                    mng.state.lock().unwrap().steps,
                    mng.buf
                );
*/
                // TOO LOOOOOOONG
                if mng.state.lock().unwrap().steps >= 10000 {
                    return;
                }

                updated = false;

                if mng.buf.iter().all(|b| b.clone().map(|b| b.1) == Some(Halt)) {
                    mng.ack.iter().for_each(|a| a.send(()).unwrap());
                    break;
                }

                if mng.buf
                    .iter()
                    .all(|b| b.clone().map(|b| b.1) == Some(Barrier))
                {
                    for r in mng.buf.iter_mut() {
                        *r = None;
                    }
                    mng.ack.iter().for_each(|a| a.send(()).unwrap());
                    updated = true;
                    continue;
                }

                let mut vola = BTreeMap::<Pos, usize>::new();
                let mut gfill_range = BTreeSet::<(Pos, Pos)>::new();
                let mut gvoid_range = BTreeSet::<(Pos, Pos)>::new();

                // fill
                for i in 0..mng.recv.len() {
                    if mng.buf[i].is_none() {
                        assert!(mng.dpos[i] == (0, 0, 0));
                        mng.buf[i] = Some(mng.recv[i].recv().unwrap());
                        updated = true;
                        // println!("FILL: {:?}", mng.buf[i]);
                    }
                }

                // println!("* {:?}", mng.buf);
                // println!("# {:?}", mng.dpos);

                for i in 0..mng.buf.len() {
                    vola.insert(vadd(&mng.buf[i].clone().unwrap().0, &mng.dpos[i]), 1);
                }

                for i in 0..mng.recv.len() {
                    let (pos, c) = mng.buf[i].clone().unwrap();
                    let pos = vadd(&pos, &mng.dpos[i]);

                    if c == Barrier || c == Halt {
                        mng.push(Wait);
                        continue;
                    }

                    // Volatileチェックを行い、ダメなら待つ
                    let cur_vola = loop {
                        // GFill/GVoid は一個目だけ
                        if let GFill(nd, fd) = c.clone() {
                            let r = normalize_range(&(pos + &nd), &(pos + &nd + &fd));
                            if gfill_range.contains(&r) {
                                break vec![];
                            } else {
                                gfill_range.insert(r);
                            }
                        }
                        if let GVoid(nd, fd) = c.clone() {
                            let r = normalize_range(&(pos + &nd), &(pos + &nd + &fd));
                            if gvoid_range.contains(&r) {
                                break vec![];
                            } else {
                                gvoid_range.insert(r);
                            }
                        }

                        break c.volatile_pos(&pos);
                    };

                    // println!(
                    //     "Command: {:?}, pos = {:?}, dpos = {:?}",
                    //     c, pos, mng.dpos[i]
                    // );

                    let is_move = if let SMove(_) = &c {
                        true
                    } else if let LMove(_, _) = &c {
                        true
                    } else {
                        false
                    };

                    let mut volatile_ok = cur_vola.iter().all(|p| {
                        ((p == &pos && vola[&p] <= 1) || !vola.contains_key(p))
                            && (!is_move
                                || !mng.state.lock().unwrap().matrix[p.0 as usize][p.1 as usize]
                                    [p.2 as usize])
                    });

                    if let GVoid(_, _) = &c {
                        volatile_ok = true; // ?
                    }

                    // println!("[{}]: {:?}", i, volatile_ok);
                    if !volatile_ok {
                        // ダメなとき、デッドロック解消のために適当に動いてみる
                        let moved = loop {
                            if rand::random::<f64>() < 0.75 {
                                break false;
                            }

                            let lld = if let SMove(lld) = &c {
                                lld
                            } else {
                                break false;
                            };

                            let ve = &[
                                (0, 0, 1),
                                (0, 0, -1),
                                (0, 1, 0),
                                (0, -1, 0),
                                (1, 0, 0),
                                (-1, 0, 0),
                            ];
                            let vv = if mng.dpos[i] == (0, 0, 0) {
                                let dir = rand::thread_rng().gen_range(0, 6);
                                ve[dir]
                            } else {
                                let p = mng.dpos[i];
                                let rev = if rand::random::<f64>() < 0.25 { 1 } else { -1 };
                                (sign(p.0) * rev, sign(p.1) * rev, sign(p.2) * rev)
                            };

                            {
                                let tt = vadd(&mng.dpos[i], &vv);
                                if tt.0.abs() + tt.1.abs() + tt.2.abs() > 5 {
                                    break false;
                                }
                            }

                            let cur = pos;
                            let next = vadd(&cur, &vv);
                            let tmp = next + lld;

                            let r = mng.state.lock().unwrap().r as i32;
                            if in_range(&next, r)
                                && in_range(&tmp, r)
                                && vola[&cur] <= 1
                                && !vola.contains_key(&next)
                                && !mng.state.lock().unwrap().matrix[next.0 as usize]
                                    [next.1 as usize][next.2 as usize]
                            {
                                // println!("Setsudou: {:?} -> {:?}", cur, next);

                                mng.push(SMove(LLD::from_pos(vv)));
                                *vola.entry(cur).or_insert(0) += 1;
                                *vola.entry(next).or_insert(0) += 1;
                                mng.dpos[i] = vadd(&mng.dpos[i], &vv);
                                break true;
                            }

                            break false;
                        };

                        if !moved {
                            mng.push(Wait);
                        }
                        continue;
                    }

                    updated = true;
                    for vo in cur_vola.iter() {
                        *vola.entry(*vo).or_insert(0) += 1;
                    }

                    mng.push(c.clone());

                    if mng.dpos[i] != (0, 0, 0) {
                        let npos = if let SMove(lld) = &c {
                            pos + lld
                        } else if let LMove(sld1, sld2) = &c {
                            pos + sld1 + sld2
                        } else {
                            pos
                        };
                        mng.buf[i] = Some((npos, SMove(LLD::from_pos(vmul(&mng.dpos[i], -1)))));
                        mng.dpos[i] = (0, 0, 0);
                    } else {
                        mng.buf[i] = None;
                        mng.ack[i].send(()).unwrap();
                    }
                }
            }

            {
                let mng = mng.lock().unwrap();
                assert!(mng.state.lock().unwrap().harmonics == false);
            }

            // Epilogue code
            let mut mng = mng.lock().unwrap();
            let n = mng.recv.len();

            for i in (1..n).rev() {
                for _ in 0..i - 1 {
                    mng.push(Wait);
                }
                mng.push(FusionP(ND::from_pos(fdir.clone())));
                mng.push(FusionS(ND::from_pos(vmul(&fdir, -1))));
            }

            mng.push(Halt);
        })
    }
}

struct Builder {
    cur: Pos,
    id: usize,
    seeds: Vec<usize>,
    send: Sender<(Pos, Command)>,
    ack: Receiver<()>,
    state: Arc<Mutex<State>>,
    tgt: Arc<Vec<Vec<Vec<bool>>>>,
    tasks: Arc<Mutex<Vec<(i32, i32)>>>,
}

impl Builder {
    fn new(
        init_pos: &Pos,
        seeds: &[usize],
        send: Sender<(Pos, Command)>,
        ack: Receiver<()>,
        state: Arc<Mutex<State>>,
        tgt: Arc<Vec<Vec<Vec<bool>>>>,
        tasks: Arc<Mutex<Vec<(i32, i32)>>>,
    ) -> Builder {
        assert!(seeds.len() >= 1);
        Builder {
            cur: *init_pos,
            id: seeds[0],
            seeds: seeds[1..].to_vec(),
            send,
            ack,
            state,
            tgt,
            tasks,
        }
    }

    fn start<F: FnOnce(&mut Builder) -> () + Copy + Send + 'static>(
        state: Arc<Mutex<State>>,
        tgt: Arc<Vec<Vec<Vec<bool>>>>,
        thread_num: usize,
        init_axis: usize,
        f: F,
    ) -> (Arc<Mutex<Manager>>, JoinHandle<()>, Vec<JoinHandle<()>>) {
        let mng = Arc::new(Mutex::new(Manager::new(state.clone())));

        let tasks = {
            let r = tgt.len() as i32;
            let mut tasks = vec![];
            for z in 0..r {
                let ox = &[3, 1, 4, 2, 0];
                let mut x = ox[z as usize % 5];
                while x < r {
                    let mut ps = 0;
                    for y in 0..r {
                        for (dx, dz) in &[(0, 1), (0, -1), (1, 0), (-1, 0)] {
                            let cx = x + dx;
                            let cz = z + dz;

                            if !(cx >= 0 && cx < r && cz >= 0 && cz < r) {
                                continue;
                            }
                            if tgt[cx as usize][y as usize][cz as usize] {
                                ps += 1;
                            }
                        }
                    }

                    if ps > 0 {
                        tasks.push((x, z));
                    }
                    x += 5;
                }
            }

            // TODO: どういう順がいい？
            // 足がついてるところの順がいいと思う

            // くっついた方がいいと思うので、近いところ順になるように
            tasks.sort_by(|a, b| {
                ((a.0 - r / 2).abs() + (a.1 - r / 2).abs())
                    .cmp(&((b.0 - r / 2).abs() + (b.1 - r / 2).abs()))
            });

            // tasks.sort_by(|a, b| (a.0 - a.1).cmp(&(b.0 - b.1)));
            // tasks.sort();

            Arc::new(Mutex::new(tasks))
        };

        let ths = (0..thread_num)
            .map(|i| {
                let (send, recv) = channel();
                let (asend, arecv) = channel();
                mng.lock().unwrap().add_builder(recv, asend);
                let state = state.clone();
                let tgt = tgt.clone();
                let tasks = tasks.clone();

                thread::spawn(move || {
                    let init_pos = match init_axis {
                        0 => (i as i32, 0, 0),
                        1 => (0, i as i32, 0),
                        2 => (0, 0, i as i32),
                        _ => unreachable!(),
                    };
                    let mut b = Builder::new(&init_pos, &[i], send, arecv, state, tgt, tasks);
                    f(&mut b);
                    assert!(
                        b.cur == init_pos,
                        "Builder[{}]: Builder must halt at initial pos. end pos = {:?}",
                        i,
                        b.cur
                    );
                    b.command(Halt);
                })
            })
            .collect();
        let th = Manager::start(init_axis, mng.clone());

        (mng, th, ths)
    }

    fn command(&mut self, c: Command) {
        // if let GVoid(_, _) = &c {
        //     println!("[{}]: GV enter, {:?}", self.id, c);
        // }

        self.send.send((self.cur, c.clone())).unwrap();
        match &c {
            SMove(lld) => {
                self.cur = self.cur + lld;
                assert!(self.cur.0 >= 0);
                assert!(self.cur.1 >= 0);
                assert!(self.cur.2 >= 0);
            }
            LMove(sld1, sld2) => {
                self.cur = self.cur + sld1;
                self.cur = self.cur + sld2;
            }
            _ => (),
        }
        self.ack.recv().unwrap();

        // if let GVoid(_, _) = &c {
        //     println!("[{}]: GV leave, {:?}", self.id, c);
        // }
    }

    fn move_to_xyz(&mut self, to: &Pos) {
        while &self.cur != to {
            let v = vect(&self.cur, to);

            if v.0 != 0 {
                let d = v.0.min(15).max(-15);
                self.command(SMove(LLD::new(d, 0, 0)));
                continue;
            }
            if v.1 != 0 {
                let d = v.1.min(15).max(-15);
                self.command(SMove(LLD::new(0, d, 0)));
                continue;
            }
            if v.2 != 0 {
                let d = v.2.min(15).max(-15);
                self.command(SMove(LLD::new(0, 0, d)));
                continue;
            }
        }
    }

    fn move_to_xzy(&mut self, to: &Pos) {
        while &self.cur != to {
            let v = vect(&self.cur, to);

            if v.0 != 0 {
                let d = v.0.min(15).max(-15);
                self.command(SMove(LLD::new(d, 0, 0)));
                continue;
            }
            if v.2 != 0 {
                let d = v.2.min(15).max(-15);
                self.command(SMove(LLD::new(0, 0, d)));
                continue;
            }
            if v.1 != 0 {
                let d = v.1.min(15).max(-15);
                self.command(SMove(LLD::new(0, d, 0)));
                continue;
            }
        }
    }

    fn move_to_yxz(&mut self, to: &Pos) {
        while &self.cur != to {
            let v = vect(&self.cur, to);

            if v.1 != 0 {
                let d = v.1.min(15).max(-15);
                self.command(SMove(LLD::new(0, d, 0)));
                continue;
            }
            if v.0 != 0 {
                let d = v.0.min(15).max(-15);
                self.command(SMove(LLD::new(d, 0, 0)));
                continue;
            }
            if v.2 != 0 {
                let d = v.2.min(15).max(-15);
                self.command(SMove(LLD::new(0, 0, d)));
                continue;
            }
        }
    }

    fn move_to_yzx(&mut self, to: &Pos) {
        while &self.cur != to {
            let v = vect(&self.cur, to);

            if v.1 != 0 {
                let d = v.1.min(15).max(-15);
                self.command(SMove(LLD::new(0, d, 0)));
                continue;
            }
            if v.2 != 0 {
                let d = v.2.min(15).max(-15);
                self.command(SMove(LLD::new(0, 0, d)));
                continue;
            }
            if v.0 != 0 {
                let d = v.0.min(15).max(-15);
                self.command(SMove(LLD::new(d, 0, 0)));
                continue;
            }
        }
    }

    fn move_to_zyx(&mut self, to: &Pos) {
        while &self.cur != to {
            let v = vect(&self.cur, to);

            if v.2 != 0 {
                let d = v.2.min(15).max(-15);
                self.command(SMove(LLD::new(0, 0, d)));
                continue;
            }
            if v.1 != 0 {
                let d = v.1.min(15).max(-15);
                self.command(SMove(LLD::new(0, d, 0)));
                continue;
            }
            if v.0 != 0 {
                let d = v.0.min(15).max(-15);
                self.command(SMove(LLD::new(d, 0, 0)));
                continue;
            }
        }
    }

    fn move_to_zxy(&mut self, to: &Pos) {
        while &self.cur != to {
            let v = vect(&self.cur, to);

            if v.2 != 0 {
                let d = v.2.min(15).max(-15);
                self.command(SMove(LLD::new(0, 0, d)));
                continue;
            }
            if v.0 != 0 {
                let d = v.0.min(15).max(-15);
                self.command(SMove(LLD::new(d, 0, 0)));
                continue;
            }
            if v.1 != 0 {
                let d = v.1.min(15).max(-15);
                self.command(SMove(LLD::new(0, d, 0)));
                continue;
            }
        }
    }

    fn move_and_void(&mut self, to: &Pos) {
        while &self.cur != to {
            let cur = self.cur;
            let v = vect(&cur, to);

            if v.1 != 0 {
                let d = v.1.min(15).max(-15);
                self.move_line_and_void(&vadd(&cur, &(0, d, 0)));
                continue;
            }
            if v.2 != 0 {
                let d = v.2.min(15).max(-15);
                self.move_line_and_void(&vadd(&cur, &(0, 0, d)));
                continue;
            }
            if v.0 != 0 {
                let d = v.0.min(15).max(-15);
                self.move_line_and_void(&vadd(&cur, &(d, 0, 0)));
                continue;
            }
        }
    }

    fn move_and_void_r(&mut self, to: &Pos) {
        while &self.cur != to {
            let cur = self.cur;
            let v = vect(&cur, to);

            if v.0 != 0 {
                let d = v.0.min(15).max(-15);
                self.move_line_and_void(&vadd(&cur, &(d, 0, 0)));
                continue;
            }
            if v.1 != 0 {
                let d = v.1.min(15).max(-15);
                self.move_line_and_void(&vadd(&cur, &(0, d, 0)));
                continue;
            }
            if v.2 != 0 {
                let d = v.2.min(15).max(-15);
                self.move_line_and_void(&vadd(&cur, &(0, 0, d)));
                continue;
            }
        }
    }

    fn move_line_and_void(&mut self, to: &Pos) {
        while &self.cur != to {
            let v = vect(&self.cur, to);
            let dv = (sign(v.0), sign(v.1), sign(v.2));
            let len = v.0.abs().max(v.1.abs()).max(v.2.abs());

            let wall = {
                let state = self.state.lock().unwrap();

                let mut wall = None;

                for i in 1..=len {
                    let p = vadd(&self.cur, &vmul(&dv, i));
                    assert!(
                        p.0 >= 0
                            && p.0 < state.matrix.len() as i32
                            && p.1 >= 0
                            && p.1 < state.matrix.len() as i32
                            && p.2 >= 0
                            && p.2 < state.matrix.len() as i32,
                        "Move out of range: {:?} -> {:?}",
                        self.cur,
                        to
                    );
                    if state.matrix[p.0 as usize][p.1 as usize][p.2 as usize] {
                        wall = Some(i);
                        break;
                    }
                }

                wall
            };

            if let Some(i) = wall {
                if i >= 2 {
                    self.command(SMove(LLD::from_pos(vmul(&dv, i - 1))));
                }
                self.command(Void(ND::from_pos(dv)));
            } else {
                self.command(SMove(LLD::from_pos(vmul(&dv, len))));
            }
        }
    }

    fn barrier(&mut self) {
        self.command(Barrier);
    }

    fn gvoid(&mut self, id: usize, r: &(Pos, Pos)) {
        let (p, q) = normalize_range(&r.0, &r.1);

        let my_pos = (
            if id & 1 == 0 { p.0 - 1 } else { q.0 + 1 },
            if id & 2 == 0 { p.1 } else { q.1 },
            if id & 4 == 0 { p.2 - 1 } else { q.2 + 1 },
        );
        let gvoid_pos = (
            if id & 1 == 0 { p.0 } else { q.0 },
            if id & 2 == 0 { p.1 } else { q.1 },
            if id & 4 == 0 { p.2 } else { q.2 },
        );
        let gvoid_opp = (
            if id & 1 != 0 { p.0 } else { q.0 },
            if id & 2 != 0 { p.1 } else { q.1 },
            if id & 4 != 0 { p.2 } else { q.2 },
        );

        // println!(
        //     "Try to GVoid: [{:?}, {:?}] => {:?} {:?} {:?}",
        //     p, q, my_pos, gvoid_pos, gvoid_opp
        // );

        if rand::random() {
            self.move_and_void(&my_pos);
        } else {
            self.move_and_void_r(&my_pos);
        }
        self.barrier();

        let nd = vect(&my_pos, &gvoid_pos);
        let fd = vect(&gvoid_pos, &gvoid_opp);

        assert!(nd != (0, 0, 0));

        // println!(
        //     "[{}]: gvoid invoke: {:?} {:?}",
        //     self.id,
        //     ND::new(nd.0, nd.1, nd.2),
        //     FD::new(fd.0, fd.1, fd.2)
        // );
        self.command(GVoid(ND::new(nd.0, nd.1, nd.2), FD::new(fd.0, fd.1, fd.2)));
        // println!("[{}]: gvoid done", self.id);
    }

    fn fill(&mut self, p: &Pos) {
        self.command(Fill(ND::from_pos(*p)));
    }

    fn mat(&self, x: usize, y: usize, z: usize) -> bool {
        self.state.lock().unwrap().matrix[x][y][z]
    }

    fn destroy_all(&mut self) {
        let id = self.id;
        let r = self.state.lock().unwrap().r as i32;
        // eprintln!("Builder: {:?}", id);

        // FIXME: デッドロック解消する

        let mut min_x = r;
        let mut max_x = 0;
        let mut min_y = r;
        let mut max_y = 0;
        let mut min_z = r;
        let mut max_z = 0;

        {
            let lock = self.state.lock().unwrap();
            for x in 0..r {
                for y in 0..r {
                    for z in 0..r {
                        if lock.matrix[x as usize][y as usize][z as usize] {
                            min_x = min_x.min(x);
                            max_x = max_x.max(x);
                            min_y = min_y.min(y);
                            max_y = max_y.max(y);
                            min_z = min_z.min(z);
                            max_z = max_z.max(z);
                        }
                    }
                }
            }
        }

        let rx = max_x - min_x + 1;
        let ry = max_y - min_y + 1;
        let rz = max_z - min_z + 1;

        let phase_x = (rx + 29) / 30;
        let phase_y = (ry + 29) / 30;
        let phase_z = (rz + 29) / 30;

        let block_x = (rx + phase_x - 1) / phase_x;
        let block_y = (ry + phase_y - 1) / phase_y;
        let block_z = (rz + phase_z - 1) / phase_z;

        let mut blocks = vec![];
        for py in 0..phase_y {
            let zs: Vec<_> = if py % 2 == 0 {
                (0..phase_z).collect()
            } else {
                (0..phase_z).rev().collect()
            };
            for pz in zs {
                let xs: Vec<_> = if (py + pz) % 2 == 0 {
                    (0..phase_x).collect()
                } else {
                    (0..phase_x).rev().collect()
                };
                for px in xs {
                    blocks.push((px, py, pz));
                }
            }
        }

        blocks.reverse();

        // 隣接ブロックにいくことを一応確認しておく
        for i in 0..blocks.len() - 1 {
            let v = vect(&blocks[i], &blocks[i + 1]);
            assert!(
                v.0.abs() + v.1.abs() + v.2.abs() == 1,
                "Must be adjacent: {:?} {:?}",
                blocks[i],
                blocks[i + 1]
            );
        }

        // println!("blocks[{}]: {}", self.id, blocks.len());

        for (px, py, pz) in blocks {
            let bx = min_x + px * block_x;
            let by = min_y + py * block_y;
            let bz = min_z + pz * block_z;

            let cx = (bx + block_x).min(r - 2);
            let cy = (by + block_y).min(r - 2);
            let cz = (bz + block_z).min(r - 2);

            // println!("block[{}]: {:?}", self.id, (px, py, pz));

            self.gvoid(id, &((bx, by, bz), (cx, cy, cz)));

            // println!("done[{}]: {:?}", self.id, (px, py, pz));
        }
    }

    fn assemble(&mut self, _num_worker: usize) {
        let id = self.id as i32;
        let r = self.state.lock().unwrap().r as i32;

        let init_pos = (id, 0, 0);
        self.move_to_zxy(&init_pos);

        // TODO: 天井の高さちゃんとはかる
        let mut ceil = 0;
        for x in 0..r {
            for y in 0..r {
                for z in 0..r {
                    if self.tgt[x as usize][y as usize][z as usize] {
                        ceil = ceil.max(y + 1);
                    }
                }
            }
        }
        // ceil = ceil.min(r - 1);

        // 本体
        while let Some((bx, bz)) = {
            let mut lock = self.tasks.lock().unwrap();
            lock.pop()
        } {
            self.move_to_yzx(&(bx, rand::thread_rng().gen_range(ceil, r), bz));
            for y in 0..r {
                for (dx, dz) in &[(0, 0), (0, 1), (0, -1), (1, 0), (-1, 0)] {
                    let x = bx + dx;
                    let z = bz + dz;

                    if !(x >= 0 && x < r && z >= 0 && z < r) {
                        continue;
                    }

                    if self.tgt[x as usize][y as usize][z as usize] {
                        if id % 2 == 0 {
                            self.move_to_yxz(&(bx, y + 1, bz));
                        } else {
                            self.move_to_yzx(&(bx, y + 1, bz));
                        }
                        self.fill(&(*dx, -1, *dz));
                    }
                }
            }
        }
        let tos = (self.cur.0, r - 1, self.cur.2);
        self.move_to_yxz(&tos);

        // 後処理
        self.move_to_zxy(&(id as i32, 0, 0));
    }
}

fn solve_full(src: Option<&Model>, tgt: Option<&Model>) -> Result<Vec<Command>> {
    if tgt.is_none() {
        return solve_destroy(src.unwrap());
    } else if src.is_none() {
        return solve_assemble(tgt.unwrap());
    } else {
        return solve_reconstruct(src.unwrap(), tgt.unwrap());
    }
}

fn solve_destroy(src: &Model) -> Result<Vec<Command>> {
    let num_worker = 8;

    let state = Arc::new(Mutex::new(State::new(
        src.r,
        src.bd.clone(),
        vec![],
        40,
        false,
    )));

    let tgt = Arc::new(vec![]);

    let (mng, wth, _) = Builder::start(state, tgt, num_worker, 1, move |b| {
        let id = b.id;
        b.destroy_all();
        b.move_to_yzx(&(0, id as i32, 0));
    });

    {
        wth.join().unwrap();
        let mng = mng.lock().unwrap();
        // println!("Generated trace: {:?}", mng.trace);
        if mng.trace[mng.trace.len() - 1] != Halt {
            // Invalid
            Err("failed")?;
        }
        Ok(mng.trace.clone())
    }
}

fn solve_assemble(tgt: &Model) -> Result<Vec<Command>> {
    let r = tgt.r;
    let num_worker = 40.min(r / 2);

    let state = Arc::new(Mutex::new(State::new(
        r,
        vec![vec![vec![false; r]; r]; r],
        vec![],
        40,
        true,
    )));

    let (mng, wth, _) = Builder::start(state, Arc::new(tgt.bd.clone()), num_worker, 0, move |b| {
        b.assemble(num_worker);
    });

    {
        wth.join().unwrap();
        let mng = mng.lock().unwrap();
        // println!("Generated trace: {:?}", mng.trace);
        // fs::write("tmp.nbt", &encode_commands(&mng.trace.clone())).unwrap();
        if mng.trace[mng.trace.len() - 1] != Halt {
            // Invalid
            Err("failed")?;
        }
        Ok(mng.trace.clone())
    }
}

fn solve_reconstruct(src: &Model, tgt: &Model) -> Result<Vec<Command>> {
    // println!("reconstruct!");

    let r = src.r as i32;

    let num_worker = 40.min(r / 2) as usize;
    // let num_worker = 8 as usize;

    let state = Arc::new(Mutex::new(State::new(
        src.r,
        src.bd.clone(),
        vec![],
        40,
        false, // 削除が終わり次第tueにしないといけない
    )));

    let mut min_x = r;
    let mut max_x = 0;
    let mut min_y = r;
    let mut max_y = 0;
    let mut min_z = r;
    let mut max_z = 0;

    for x in 0..r {
        for y in 0..r {
            for z in 0..r {
                if src.bd[x as usize][y as usize][z as usize] {
                    min_x = min_x.min(x);
                    max_x = max_x.max(x);
                    min_y = min_y.min(y);
                    max_y = max_y.max(y);
                    min_z = min_z.min(z);
                    max_z = max_z.max(z);
                }
            }
        }
    }

    let rx = max_x - min_x + 1;
    let ry = max_y - min_y + 1;
    let rz = max_z - min_z + 1;

    let phase_x = (rx + 29) / 30;
    let phase_y = (ry + 29) / 30;
    let phase_z = (rz + 29) / 30;

    let mut ceil = 0;
    for x in 0..r {
        for y in 0..r {
            for z in 0..r {
                if tgt.bd[x as usize][y as usize][z as usize] {
                    ceil = ceil.max(y + 1);
                }
            }
        }
    }

    let (mng, wth, _) = Builder::start(state, Arc::new(tgt.bd.clone()), num_worker, 1, move |b| {
        let id = b.id;

        // 破壊
        if id < 8 {
            b.barrier();
            b.destroy_all();
        } else {
            // let phase = (r - 2 + 29) / 30;

            // 他の人の邪魔にならないようにしておく
            b.move_to_yxz(&(
                2 + id as i32 / 7 * 2,
                rand::thread_rng().gen_range(ceil, r),
                2 + id as i32 % 7 * 2,
            ));
            // b.move_to_yxz(&(1, id as i32, 0));
            b.barrier();

            // barrierをgfillするのと同じ回数やらなければいけない
            let gfill_num = phase_x * phase_y * phase_z;
            for _ in 0..gfill_num {
                b.barrier();
            }
        }

        b.barrier();

        // 再生
        b.move_to_yzx(&(0, id as i32, 0));
        b.move_to_xyz(&(id as i32, 0, 0));
        // (id, 0, 0) からスタート
        b.assemble(num_worker);
        b.barrier();
        // (id, 0, 0) に戻っているから、本来の最初の位置に戻る
        b.move_to_yxz(&(0, id as i32, 0));
    });

    {
        wth.join().unwrap();
        let mng = mng.lock().unwrap();
        // println!("Generated trace: {:?}", mng.trace);
        if mng.trace[mng.trace.len() - 1] != Halt {
            // Invalid
            Err("failed")?;
        }
        Ok(mng.trace.clone())
    }
}

fn vect(from: &Pos, to: &Pos) -> Pos {
    (to.0 - from.0, to.1 - from.1, to.2 - from.2)
}

fn vadd(from: &Pos, v: &Pos) -> Pos {
    (from.0 + v.0, from.1 + v.1, from.2 + v.2)
}

fn vmul(from: &Pos, m: i32) -> Pos {
    (from.0 * m, from.1 * m, from.2 * m)
}

fn sign(n: i32) -> i32 {
    if n == 0 {
        0
    } else if n > 0 {
        1
    } else {
        -1
    }
}

fn in_range(p: &Pos, r: i32) -> bool {
    p.0 >= 0 && p.0 < r && p.1 >= 0 && p.1 < r && p.2 >= 0 && p.2 < r
}

//---------
// Storage

const LIGHTNING_DIR: &str = "results";
const FULL_DIR: &str = "results_full";

fn touch_dir<P: AsRef<Path>>(path: P) -> Result<()> {
    let path = path.as_ref();
    if !path.exists() {
        fs::create_dir_all(path)?;
    }
    Ok(())
}

fn get_result_dir(root: &str, name: &str) -> Result<PathBuf> {
    let pb = Path::new(root).join(name);
    touch_dir(&pb)?;
    Ok(pb)
}

fn get_best_score(root: &str, name: &str) -> Result<Option<i64>> {
    let pb = get_result_dir(root, name)?;

    let re2 = Regex::new(r"/.+/(\d+)\.nbt")?;
    let mut results: Vec<(i64, String)> = fs::read_dir(&pb)?
        .map(|e| {
            let s = e.unwrap().path().to_string_lossy().into_owned();
            let n = {
                let caps = re2.captures(&s).unwrap();
                caps.get(1).unwrap().as_str().parse().unwrap()
            };
            (n, s)
        })
        .collect();
    results.sort();

    Ok(if results.len() == 0 {
        None
    } else {
        Some(results[0].0)
    })
}

fn save_solution(root: &str, name: &str, trace: &[Command], score: i64) -> Result<()> {
    // println!("*****: {:?} {:?} {:?} {}", root, name, score);

    let best = get_best_score(root, name)?.unwrap_or(0);
    if best == 0 || best > score {
        println!("* Best score for {}: {} -> {}", name, best, score);
        let pb = get_result_dir(root, name)?;
        fs::write(pb.join(format!("{}.nbt", score)), &encode_commands(trace))?;
    }
    Ok(())
}

//--------
// Driver

#[derive(StructOpt, Debug)]
#[structopt(name = "basic")]
enum Opt {
    #[structopt(name = "decode")]
    Decode {
        #[structopt(parse(from_os_str))]
        path: PathBuf,
    },

    #[structopt(name = "eval")]
    Eval {
        #[structopt(parse(from_os_str))]
        model: PathBuf,
        #[structopt(parse(from_os_str))]
        trace: PathBuf,
    },

    #[structopt(name = "evalf")]
    EvalF {
        #[structopt(long = "src", parse(from_os_str))]
        src: Option<PathBuf>,
        #[structopt(long = "tgt", parse(from_os_str))]
        tgt: Option<PathBuf>,
        #[structopt(parse(from_os_str))]
        trace: PathBuf,
    },

    #[structopt(name = "solve")]
    Solve {
        #[structopt(parse(from_os_str))]
        model: PathBuf,
    },

    #[structopt(name = "solvef")]
    SolveF {
        #[structopt(long = "src", parse(from_os_str))]
        src: Option<PathBuf>,
        #[structopt(long = "tgt", parse(from_os_str))]
        tgt: Option<PathBuf>,
    },

    #[structopt(name = "pack")]
    Pack {
        #[structopt(long = "lightning")]
        lightning: bool,
    },

    #[structopt(name = "check")]
    Check {
        #[structopt(long = "lightning")]
        lightning: bool,
        #[structopt(long = "ty")]
        ty: Option<char>,
    },

    #[structopt(name = "rest")]
    Rest,
}

fn get_model_name<P: AsRef<Path>>(model: P) -> String {
    let re = Regex::new(r".*/((\d|\w)+)_tgt\.mdl").unwrap();
    let t = model.as_ref().to_string_lossy();
    let caps = re.captures(&t).unwrap();
    caps.get(1).unwrap().as_str().to_owned()
}

fn get_model_name_full<P: AsRef<Path>, Q: AsRef<Path>>(src: Option<P>, tgt: Option<Q>) -> String {
    let re_src = Regex::new(r".*/((\d|\w)+)_src\.mdl").unwrap();
    let re_tgt = Regex::new(r".*/((\d|\w)+)_tgt\.mdl").unwrap();

    let sname = src.map(|s| {
        let t = s.as_ref().to_string_lossy();
        let caps = re_src.captures(&t).unwrap();
        caps.get(1).unwrap().as_str().to_owned()
    });

    let tname = tgt.map(|s| {
        let t = s.as_ref().to_string_lossy();
        let caps = re_tgt.captures(&t).unwrap();
        caps.get(1).unwrap().as_str().to_owned()
    });

    match (sname, tname) {
        (Some(sname), None) => sname,
        (None, Some(tname)) => tname,
        (Some(sname), Some(tname)) => {
            assert!(sname == tname);
            sname
        }
        _ => {
            panic!("Source and target are both empty");
        }
    }
}

fn round_trip_test(cmds: &[Command]) {
    let t: Vec<u8> = encode_commands(&cmds);
    let u: Vec<Command> = decode_commands(&t);

    assert_eq!(cmds.len(), u.len());

    for i in 0..cmds.len() {
        assert_eq!(cmds[i], u[i]);
    }
}

fn main() -> Result<()> {
    let opt = Opt::from_args();
    match opt {
        Opt::Decode { path } => {
            eprintln!("Decode: Path = {:?}", path);
            let cs = decode_commands(&fs::read(&path)?);

            println!("#Commands: {}", cs.len());
            println!("Commands:");
            for c in cs {
                println!("    {:?}", c);
            }
        }

        Opt::Eval { model, trace } => {
            let name = get_model_name(&model);
            // eprintln!("Eval: model = {:?}, trace = {:?}", model, trace);
            let model = Model::decode(&fs::read(&model)?);
            let trace = decode_commands(&fs::read(&trace)?);
            eval_lightning(&name, &model, &trace)?;
        }

        Opt::EvalF { src, tgt, trace } => {
            let name = get_model_name_full(src.as_ref(), tgt.as_ref());
            // eprintln!(
            //     "Eval: src = {:?}, tgt = {:?}, trace = {:?}",
            //     src, tgt, trace
            // );
            let src = src.map(|p| Model::decode(&fs::read(p).unwrap()));
            let tgt = tgt.map(|p| Model::decode(&fs::read(p).unwrap()));
            let trace = decode_commands(&fs::read(&trace)?);
            eval_full(&name, src.as_ref(), tgt.as_ref(), &trace)?;
        }

        Opt::Solve { model } => {
            let model_id = get_model_name(&model);
            let model = Model::decode(&fs::read(&model)?);

            for b in 0..24 {
                let b0 = b / 8;
                let b1 = if b % 8 == 0 { 0 } else { 1 << (b % 8 - 1) };

                let cmds = solve(&model, b0, b1);
                if cmds.len() == 0 {
                    continue;
                }

                round_trip_test(&cmds);
                eval_lightning(&model_id, &model, &cmds)?;
            }
        }

        Opt::SolveF { src, tgt } => {
            let name = get_model_name_full(src.as_ref(), tgt.as_ref());
            let src = src.map(|p| Model::decode(&fs::read(p).unwrap()));
            let tgt = tgt.map(|p| Model::decode(&fs::read(p).unwrap()));

            for _ in 0..5 {
                let trace = solve_full(src.as_ref(), tgt.as_ref());
                if let Ok(trace) = trace {
                    round_trip_test(&trace);
                    eval_full(&name, src.as_ref(), tgt.as_ref(), &trace)?;
                } else {
                    println!("Failed: {}", name);
                }
            }
        }

        Opt::Pack { lightning } => {
            let dir = if lightning { LIGHTNING_DIR } else { FULL_DIR };

            let tmp = Path::new("tmp");
            if tmp.is_dir() {
                fs::remove_dir_all(tmp)?;
            }
            fs::create_dir_all(tmp)?;

            let mut files = vec![];

            let re = Regex::new(r"[a-z_]+/(.+)").unwrap();
            for entry in fs::read_dir(dir)? {
                let entry = entry?;
                let path = entry.path();
                let name = {
                    let t = path.to_string_lossy();
                    let caps = re.captures(&t).unwrap();
                    caps.get(1).unwrap().as_str().to_owned()
                };

                if let Some(best) = get_best_score(dir, &name)? {
                    let fname = format!("{}.nbt", name);
                    fs::copy(path.join(format!("{}.nbt", best)), tmp.join(&fname))?;
                    files.push(fname);
                }
            }

            env::set_current_dir(tmp)?;
            std::process::Command::new("zip")
                .arg("--password")
                .arg(&PRIVATE_ID)
                .arg("../submission.zip")
                .args(&files)
                .status()?;
        }

        Opt::Rest => {
            let dir = FULL_DIR;

            let re = Regex::new(r"/(.+)").unwrap();
            for entry in fs::read_dir(dir)? {
                let entry = entry?;
                let path = entry.path();
                let name = {
                    let t = path.to_string_lossy();
                    let caps = re.captures(&t).unwrap();
                    caps.get(1).unwrap().as_str().to_owned()
                };

                if fs::read_dir(path)?.count() <= 1 {
                    println!("* {}", name);
                }
            }
        }

        Opt::Check { lightning, ty } => {
            let dir = if lightning { LIGHTNING_DIR } else { FULL_DIR };

            let re = Regex::new(r"/(.+)").unwrap();
            let mut cands = vec![];
            for entry in fs::read_dir(dir)? {
                let entry = entry?;
                let path = entry.path();
                let name = {
                    let t = path.to_string_lossy();
                    let caps = re.captures(&t).unwrap();
                    caps.get(1).unwrap().as_str().to_owned()
                };

                if let Some(best) = get_best_score(dir, &name)? {
                    cands.push((best, path, name));
                }
            }

            for (best, path, name) in cands {
                if ty.is_some() && name.chars().nth(1) != ty {
                    continue;
                }

                if lightning {
                    let model = format!("../data/model/{}_tgt.mdl", name);
                    let trace = path.join(format!("{}.nbt", best));

                    let model = Model::decode(&fs::read(&model)?);
                    let trace = decode_commands(&fs::read(&trace)?);

                    eval_lightning(&name, &model, &trace)?;
                } else {
                    let src = if name.chars().nth(1) != Some('A') {
                        Some(format!("../data/modelF/{}_src.mdl", name))
                    } else {
                        None
                    };
                    let tgt = if name.chars().nth(1) != Some('D') {
                        Some(format!("../data/modelF/{}_tgt.mdl", name))
                    } else {
                        None
                    };

                    let trace = path.join(format!("{}.nbt", best));

                    let src = src.map(|src| Model::decode(&fs::read(&src).unwrap()));
                    let tgt = tgt.map(|tgt| Model::decode(&fs::read(&tgt).unwrap()));
                    let trace_d = decode_commands(&fs::read(&trace)?);

                    if let Err(e) = eval_full(&name, src.as_ref(), tgt.as_ref(), &trace_d) {
                        fs::remove_file(&trace)?;
                        println!("* Trace {:?} is invalid. removed: {}", trace, e);
                    }
                }
            }
        }
    }

    Ok(())
}
