ICFP Programming Contest 2018

Team forM_'s submission:

# How to run solver

* For full division

> cargo run --release sovlef --src <path-to-src.mdl> --tgt <path-to-tgt.mdl>

* For lightning division

> cargo run --release solve <path-to-tgt.mdl>

# Approach

* For assembly task:

At first, by using Fission commands to produce enough amount of bots. Then, divide regions to X-Pentomino shapes and each bots take a region and fill low to high along the y-axis.

* For disassembly task:

We create 8 bots and they use GVoid command to avoid all cells. The bounding box src object are divideed evenly into cuboids. 8 bots erase each cuboids sequentially.

* For reassembly task:

Just do disassemble, then assembly.

# Feedback about the contest

I have very enjoyed to challenge to this year's task. It is so interesting and seems to be a very primitive and complicated parallel programming. The race condisions of this task is not obvious, so it is too hard to implement and debug even enough simple strategy.

Live scoreboard is good, but I wish it had refreshed more frequently. And also, I wanted to know my submission's result.

Above all, thank you for fun contest.
